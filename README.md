# Emonitor
Visualize your energy production and consumption

See the [project wiki](https://gitlab.com/technimad/emonitor/-/wikis/home) for details.

# installing
Create a config file for SBF spot. Base this config on ```cfg/SBFspot.default.cfg``` and name your copy ```cfg/SBFspot.cfg```.
In this file set:
- BTAddress to the bluetooth address of your SMA inverter
- Timezone to your local time zone
- SQL_Database to ```/cfg/SBFspot.db```

If needed we can automatically add the arp entry for your router. Add this config if your internet access failes after a reboot, but is restored after some pings.
To enable this functionality create a file ```cfg/router-ip-mac.cfg``` with the following contents:
```
ROUTER_IP=192.168.0.1
ROUTER_MAC=d4:aa:dd:84:22:cc
```
Change the values to match your local network.

Run the script ```./install.sh``` as root.

# Update screen

docker run --rm --network none -it -v $PWD/screen/src:/app -v $PWD/cfg:/cfg registry.gitlab.com/technimad/emonitor/screen:master python3 plot.py

docker run --rm -it -v $PWD/screen/src:/app -v $PWD/cfg:/cfg --privileged registry.gitlab.com/technimad/emonitor/screen:master python3 screen.py


# Backup configuration and data
```
rsync -aiv rpi:emonitor/cfg .
```