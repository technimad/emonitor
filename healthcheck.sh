#!/usr/bin/env bash
set -e          # Abort script at first error, when a command exits with non-zero status 
set -o pipefail # Causes a pipeline to return the exit status of the last command in the 
                # pipe that returned a non-zero return value.
#set -u          # Attempt to use undefined variable outputs error message, and forces an exit

DEBUG=${DEBUG:-}
if [[ ! -z $DEBUG ]]; then
  set -x
fi

cd `dirname $0`
while true; do   
  for service in $(docker ps -f "label=com.docker.compose.project=emonitor" \
                             -f "health=unhealthy" \
                             --format "{{.Labels}}" \
                            | grep -o "\(com\.docker\.compose\.service=\)[a-z0-9]*" \
                            | cut -d "=" -f2 ); do
    logger -i -t $0 "restart unhealthy service: $service"
    docker-compose restart $service
  done
  sleep 15
done