use rusqlite::Connection;
use std::{env, process::exit};

mod utils;

fn main() {
    let mut args = env::args_os().skip(1);
    let database_path = &args.next().unwrap_or_default();

    println!("Start P1 reader healthcheck with following arguments:");
    println!("Database path: {:?}", &database_path);
    let db = match Connection::open(database_path) {
        Ok(con) => con,
        Err(e) => {
            println!("Cannot open database: {}", e);
            exit(1);
        }
    };
    match utils::db::check_health(&db) {
        Ok(_) => {
            exit(0);
        }
        Err(e) => {
            println!("Unhealthy: {}", e);
            exit(255);
        }
    }
}
