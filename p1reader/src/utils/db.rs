use rusqlite::{Connection, Result};

pub const DB_SCHEMA_VER: i8 = 1;

pub fn create_schema(db: &mut Connection) -> Result<()> {
    let tx = db.transaction()?;
    let _ = &tx.execute(
        "CREATE TABLE Config (
          `Key` varchar(32),
          `Value` varchar(200),
          PRIMARY KEY (`Key`)
  );",
    (),
    )?;
    let _ = &tx.execute(
        "INSERT INTO `Config` (Key, Value)
  VALUES ('SchemaVersion', ?);",
        [DB_SCHEMA_VER],
    )?;

    let _ = &tx.execute(
        "CREATE TABLE EnergyReadings (
      `TimeStamp` datetime NOT NULL,
      `received_1` float,
      `delivered_1` float,
      `received_2` float,
      `delivered_2` float,
      `power_received` float,
      `power_delivered` float,
      `tariff` int,
      PRIMARY KEY (`TimeStamp`)
);",
        (),
    )?;

    let _ = &tx.execute(
        "CREATE VIEW vwEnergyReadings AS
        SELECT
        TimeStamp,
        received_1+received_2 as received,
        delivered_1+delivered_2 as delivered
        from EnergyReadings
        order by timestamp desc;",
        (),
    )?;
    tx.commit()?;
    Ok(())
}

#[allow(clippy::needless_return)]
pub fn check_schema(db: &Connection) -> Result<i8> {
    let mut stmt = db.prepare("Select value from Config where Key = \"SchemaVersion\"")?;

    let mut rows = stmt.query(())?;
    match rows.next() {
        Ok(row) => match row {
            Some(r) => {
                let v_str: String = r.get(0)?;
                let v: i8 = v_str.trim().parse().unwrap_or_default();
                Ok(v)
            }
            None => {
                println!("schema not valid");
                Err(rusqlite::Error::QueryReturnedNoRows)
            }
        },
        Err(e) => {
            println!("schema not valid");
            Err(e)
        }
    }
}

#[allow(clippy::needless_return)]
pub fn check_health(db: &Connection) -> Result<()> {
    let sql = "SELECT count(*) from EnergyReadings WHERE TimeStamp > strftime('%s','now') - 30 order by timestamp desc;";
    match db.query_row::<i32, _, _>(sql, (), |row| row.get(0)) {
        Ok(count) => {
            println!("count {:?}", count);
            if count > 0 {
                return Ok(());
            }
            Err(rusqlite::Error::QueryReturnedNoRows)
        }
        Err(e) => {
            println!("Healthcheck failed {}", e);
            Err(e)
        }
    }
}
