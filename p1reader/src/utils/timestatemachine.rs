use chrono::{DateTime, LocalResult, Utc};

struct TimeStateMachine {
    local_time: LocalResult<DateTime<Utc>>,
    state: TimeState,
}

enum TimeState {
    Undecided,
    Earliest,
    Latest
}