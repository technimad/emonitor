extern crate serial;

use chrono::{NaiveDate, NaiveDateTime, TimeZone, Utc};
use chrono_tz::Europe::Amsterdam;
//use rusqlite::{params, Connection, Result};
use rusqlite::{params, Connection, Result};
use std::env;
use std::io;
//use std::process;
use regex::Regex;
//use regex::*;
use serial::prelude::*;
//use std::io::prelude::*;
use std::str;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

mod utils;

#[macro_use]
extern crate lazy_static;

#[derive(Debug)]
pub struct Telegram {
    timestamp: chrono::DateTime<chrono::offset::Utc>,
    received_1: f64,      // energy delivered from net to client in kWh, tariff 1
    delivered_1: f64,     // energy delivered by client to net in kWh, tariff 1
    received_2: f64,      // energy delivered from net to client in kWh, tariff 2
    delivered_2: f64,     // energy delivered by client to net in kWh, tariff 2
    power_received: f64,  // current power feed from net to client in kW
    power_delivered: f64, // current power feed from client to net in kW
    tariff: Tariff,       //current tariff
}

#[derive(Debug, Copy, Clone)]
pub enum Tariff {
    Low = 1,
    High = 2,
}
impl rusqlite::ToSql for Tariff {
    fn to_sql(&self) -> Result<rusqlite::types::ToSqlOutput<'_>> {
        Ok(rusqlite::types::ToSqlOutput::from(*self as i8))
    }
}

impl Default for Telegram {
    fn default() -> Self {
        Self::new()
    }
}

impl Telegram {
    pub fn new() -> Self {
        Self {
            timestamp: Utc.with_ymd_and_hms(2000, 1, 1,0, 0, 0).single().unwrap(),
            received_1: 0.0,
            delivered_1: 0.0,
            received_2: 0.0,
            delivered_2: 0.0,
            power_received: 0.0,
            power_delivered: 0.0,
            tariff: Tariff::High,
        }
    }
    pub fn add(&mut self, reference: &str, value: &str, _unit: Option<&str>, _data: Option<&str>) {
        match reference {
            "0-0:1.0.0" => {
                let naive_datetime = NaiveDateTime::parse_from_str(value, "%y%m%d%H%M%S")
                    .unwrap_or(NaiveDate::with_ymd_and_hms(2000, 1, 1,0, 0, 0).single().unwrap());
                self.timestamp = Amsterdam
                    .from_local_datetime(&naive_datetime)
                    .unwrap()
                    .with_timezone(&Utc);
            }
            "1-0:1.8.1" => self.received_1 = value.parse::<f64>().unwrap_or(0.0),
            "1-0:2.8.1" => self.delivered_1 = value.parse::<f64>().unwrap_or(0.0),
            "1-0:1.8.2" => self.received_2 = value.parse::<f64>().unwrap_or(0.0),
            "1-0:2.8.2" => self.delivered_2 = value.parse::<f64>().unwrap_or(0.0),
            "1-0:1.7.0" => self.power_received = value.parse::<f64>().unwrap_or(0.0),
            "1-0:2.7.0" => self.power_delivered = value.parse::<f64>().unwrap_or(0.0),
            "0-0:96.14.0" => {
                self.tariff = match value.parse::<i8>().unwrap_or_default() {
                    1 => Tariff::Low,
                    2 => Tariff::High,
                    _ => Tariff::High,
                }
            }

            _ => (),
        }
    }

    fn from_data_string(telegram_data: String) -> Self {
        let mut telegram = Self::new();

        lazy_static! {
            static ref OBIS_REGEX: Regex = Regex::new(r"((?P<reference>[01]-[0123]:[0-9]+\.[0-9]+\.[0-9]+)(?P<data>(\((?P<val>[0-9.]*)(?P<unit>[^\)]*)\))+))").unwrap();
        }

        for mat in OBIS_REGEX.find_iter(&telegram_data) {
            for caps in OBIS_REGEX.captures_iter(mat.as_str()) {
                //&caps["reference"], &caps["val"], &caps["unit"], &caps["data"]
                telegram.add(
                    &caps["reference"],
                    &caps["val"],
                    Some(&caps["unit"]),
                    Some(&caps["data"]),
                );
                //println!("{:?}", caps);
            }
        }
        telegram
    }
}

fn main() {
    let mut args = env::args_os().skip(1);
    let serial_device_path = &args.next().unwrap_or_default();
    let database_path = &args.next().unwrap_or_default();

    println!("Start P1 reader with following arguments:");
    println!("Serial device: {:?}", &serial_device_path);
    println!("Database path: {:?}", &database_path);

    let mut port: serial::SystemPort;
    loop {
        //setup port
        port = match serial::open(&serial_device_path) {
            Ok(p) => p,
            Err(e) => {
                println!("Cannot open {:?}: {}", &serial_device_path, e);
                thread::sleep(Duration::from_millis(2000));
                continue;
            }
        };
        match port.reconfigure(&|settings| {
            settings
                .set_baud_rate(serial::Baud115200)
                .expect("Unsupported baud rate");
            settings.set_char_size(serial::Bits8);
            settings.set_parity(serial::ParityNone);
            settings.set_stop_bits(serial::Stop1);
            settings.set_flow_control(serial::FlowNone);
            Ok(())
        }) {
            Ok(_) => (),
            Err(e) => {
                println!("Cannot configure port: {}", e);
                break;
            }
        }
        match port.set_timeout(Duration::from_millis(20000)) {
            Ok(_) => (),
            Err(e) => {
                println!("Cannot set timeout on serial device: {}", e);
                break;
            }
        }

        //setup db
        let mut db = match Connection::open(database_path) {
            Ok(con) => con,
            Err(e) => {
                println!("Cannot open database: {}", e);
                break;
            }
        };
        let schema_version = match utils::db::check_schema(&db) {
            Ok(v) => v,
            Err(e) => {
                println!("Database schema not valid: {:?}", e);
                match utils::db::create_schema(&mut db) {
                    Ok(_) => {
                        println!("Schema created sucessfully.");
                        db.close().unwrap_or(());
                        drop(port); // remove exclusive lock on serial port
                        continue;
                    }
                    Err(e) => {
                        println!("Could not create schema: {}", e);
                        break;
                    }
                };
            }
        };
        if schema_version != utils::db::DB_SCHEMA_VER {
            println!(
                "Invalid db schema. Found: {}, Expected: {}",
                schema_version,
                utils::db::DB_SCHEMA_VER
            );
            break;
        }
        // force WAL mode in sqlite for better performance
        match &db.pragma_update(None, "journal_mode", &String::from("WAL")) {
            Ok(_) => (),
            Err(_) => (),
        };

        //read telegrams
        match read_telegrams(&mut port, db) {
            Ok(_) => (),
            Err(e) => {
                println!("Error while reading telegram: {:?}", &e);
                //db.close().unwrap_or_default();
                drop(port); // remove exclusive lock on serial port
                thread::sleep(Duration::from_millis(2000));
                continue;
            }
        }
    }
}

fn read_telegrams<T: SerialPort>(port: &mut T, db: Connection) -> io::Result<()> {
    let mut buf = [0; 256];
    let mut telegram_data = String::new();

    //first loop could start in the middle of a telegram,
    // so we set this flag after the first start of telegram has been seen
    let mut in_telegram = false;

    // We handle the received telegrams in separate thread to prevent
    // blocking the reads while processing the contents.
    // This thread terminates when the channel it is listening on is dropped.
    let (tx, rx) = mpsc::channel();
    let _ = thread::Builder::new()
        .name("p1parser".to_string())
        .spawn(move || loop {
            println!(
                "{:?} - waiting for telegram",
                thread::current().name().unwrap_or_default()
            );
            match rx.recv() {
                Ok(data) => {
                    let telegram = Telegram::from_data_string(data);
                    match db.execute("INSERT INTO 
                        EnergyReadings (TimeStamp, received_1, delivered_1, received_2, delivered_2, power_received, power_delivered, tariff)
                        VALUES         (?1,        ?2,         ?3,          ?4,         ?5,          ?6,             ?7,              ?8)",
                    params![telegram.timestamp.with_timezone(&Utc).timestamp(),
                            telegram.received_1,
                            telegram.delivered_1,
                            telegram.received_2,
                            telegram.delivered_2,
                            telegram.power_received,
                            telegram.power_delivered,
                            telegram.tariff]) {
                                Ok(_) => println!("telegram stored"),
                                Err(e) => println!("error storing telegram: {:?}", e)
                            }


                    println!("{:?}", telegram);
                }
                Err(e) => {
                    println!(
                        "{:?} - ending thread: {:?}",
                        thread::current().name().unwrap_or_default(),
                        e
                    );
                    break;
                }
            }
        });

    loop {
        let c = port.read(&mut buf)?;

        let words = str::from_utf8(buf.get(0..c).unwrap())
            .unwrap_or("XXX")
            .split_whitespace();
        for word in words {
            if word.starts_with('/') {
                // Start of telegram
                telegram_data = String::new();
                in_telegram = true;
            }
            telegram_data.push_str(word);

            if word.starts_with('!') && in_telegram {
                    // End of telegram
                    tx.send(telegram_data.clone()).unwrap();
            }
        }
    }
    //Ok(())
}
