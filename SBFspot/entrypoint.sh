#!/usr/bin/env bash
set -e          # Abort script at first error, when a command exits with non-zero status 
set -o pipefail # Causes a pipeline to return the exit status of the last command in the 
                # pipe that returned a non-zero return value.
set -u          # Attempt to use undefined variable outputs error message, and forces an exit

DEBUG=${DEBUG:-}
if [[ ! -z $DEBUG ]]; then
	set -x
fi

DATABASE=$(grep -i '^sql_database=' /cfg/SBFspot.cfg | cut -d "=" -f 2)

if [[ ! -f $DATABASE ]] ; then
  cp /app/smadata/SBFspot.db $DATABASE
fi

/usr/local/bin/sbfspot.3/SBFspot $@