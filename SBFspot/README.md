# Config

Create config file ```cfg/SBFspot.cfg``` based on the provided default file.
Provide config via a bind mount, i.e. ```-v "$PWD"/cfg:/cfg```

# Run
Easiest way to start is to use ```docker-compose up``` in the root directory

Run image: 
```docker run --rm -d --net=host --privileged -v /dev/bus/usb:/dev/bus/usb -v "$PWD"/cfg:/cfg --name=sbfspot registry.gitlab.com/technimad/emonitor/sbfspot:runcont-slim```

Monitor progress:
```docker logs -f sbfspot```

Stop image:
```docker kill sbfspot```

# Build
Image is build for multiple architectures.

To build for multiple architectures on macOS, use Docker edge so you have access to the command ```docker buildx```. Setup a builder for your setup, see: https://www.docker.com/blog/multi-arch-images/ for reference. 
The multiplatform images are build by this command:
```
docker buildx build --platform linux/arm/v7,linux/amd64 -t registry.gitlab.com/technimad/emonitor/sbfspot:runcont-slim --push .
```

