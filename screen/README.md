 docker run -it --rm -v $PWD/screen/src:/app -v $PWD/cfg:/cfg registry.gitlab.com/technimad/emonitor/screen:v.1.2 python3 plot.py

docker run -it -v $PWD/screen/src:/app -v $PWD/cfg:/cfg --device /dev/spidev0.0:/dev/spidev0.0 --device /dev/spidev0.1:/dev/spidev0.1 --privileged --net=host registry.gitlab.com/technimad/emonitor/screen:v.1.2 python3 screen.py