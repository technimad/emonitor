-- [6] get 20pct avg 80pct for each mins
select min(solar) as pct20,  
          avg, 
          max(solar) as pct80, 
          	strftime('%s', 'now', 'start of day')+(mins*60) as timestamp
from (

-- [5] select maximum solar production per percentile per mins
select p, avg, mins, max(solar) as solar
from (

-- [4] change moving average to max average per mins
select p, max(avg_win) over win as avg, mins, solar
from (

-- [3] add percentile groups and moving average per mins
select ntile(8) over win as p, avg(solar) over win as avg_win, mins, solar
                  from (

-- [2] add minutes of day 
select 
    strftime('%Y%j', nearestmin) as day,
    strftime('%H', nearestmin) * 60 + strftime('%M', nearestmin) as mins,
    solar
    FROM (

-- [1] compute production per 10 mins
select 
  datetime(CASE WHEN (sbfdat.TimeStamp % 600) < 300
    THEN sbfdat.TimeStamp - (sbfdat.TimeStamp % 600)
    ELSE sbfdat.TimeStamp - (sbfdat.TimeStamp % 600) + 600
    END, 'unixepoch', 'localtime') AS nearestmin, 
  max(ETotal)-lag(max(ETotal)) over (order by sbfdat.timestamp asc) as solar
  from spotdata as sbfdat 
  where 
  (sbfdat.timestamp < strftime('%s' , date('now',  'start of day')) AND sbfdat.timestamp > strftime('%s', date('now', '-7 day')))
  OR
  (sbfdat.timestamp < strftime('%s' , date('now',  '-1 year', '+3 day')) AND sbfdat.timestamp > strftime('%s', date('now', '-1 year', '-3 day')))
  OR 
    (sbfdat.timestamp < strftime('%s' , date('now',  '-2 year', '+3 day')) AND sbfdat.timestamp > strftime('%s', date('now', '-2 year', '-3 day')))

  group by nearestmin
  order by sbfdat.timestamp desc
-- end [1]
  )
-- end [2]
  ) 
  where solar > 0 
 window win as (partition by mins order by solar asc) 
 order by mins
-- end [3]
 )
 window win as (partition by mins order by solar desc)
 -- end [4]
 )
 group by mins, p
 --end [5]
 )
 where p > 1 AND p < 8
 group by mins
;