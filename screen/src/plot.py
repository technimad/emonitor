import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import matplotlib.font_manager as font_manager
import emonitor
import time


# Initialize matplotlib 
ts = time.time()
#  Setup font handing
font_path = '/usr/share/fonts/truetype/liberation2/'
font_manager.FontProperties(fname=font_path)
#  Create figure and grid and add subplots to grid
fig = plt.figure(constrained_layout=True)
gs = GridSpec(2, 6, figure=fig)

#ax1 = fig.add_subplot(gs[:-1, :])
ax1 = fig.add_subplot(gs[0, 0:3]) # left top
ax3 = fig.add_subplot(gs[0, 3:6]) # right top
ax2 = fig.add_subplot(gs[1, 0:2]) # left bottom
ax4 = fig.add_subplot(gs[1, 4:6]) # right bottom
ax5 = fig.add_subplot(gs[1, 2:4]) # middle bottom

print(time.time()-ts, " init")

# Create graphs
emonitor.draw_graph(ax1, "Laatste 16 uur", *emonitor.get_energy_data(600, 96))
emonitor.draw_graph(ax3, "Laatste 2 uur", *emonitor.get_energy_data(120, 60))
emonitor.draw_graph(ax2, "16 uur gisteren", *emonitor.get_energy_data(600, 96, 96))
emonitor.draw_graph(ax4, "Afgelopen 2 weken", *emonitor.get_energy_data(86400, 15))

#emonitor.draw_sun(ax5, "zon op      zon onder", *emonitor.predict_sun())
(sun_up, sun_max, sun_down) = emonitor.predict_sun()
from pytz import timezone
amsterdam = timezone('Europe/Amsterdam')
sun_up = sun_up.astimezone(amsterdam).strftime('%H:%M')
sun_max = sun_max.astimezone(amsterdam).strftime('%H:%M')
sun_down = sun_down.astimezone(amsterdam).strftime('%H:%M')
title = "↑"+sun_up+"   zon   "+sun_down+"↓"
emonitor.draw_reference(ax5, title, *emonitor.get_history_reference_data(bucket_size=300), *emonitor.get_current_production_current_day(bucket_size=300))

#
# Create image
ts = time.time()
plt.savefig('/cfg/output.png')
print(time.time()-ts, " savefig")
emonitor.conn.close()

# Split image by color
ts = time.time()
emonitor.split_image('/cfg/output.png')
print(time.time()-ts, " split image")