#!/usr/bin/python
# -*- coding:utf-8 -*-
import sys
import os

import logging
from waveshare_epd import epd7in5b_HD
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
import random

logging.basicConfig(level=logging.DEBUG)

try:
    epd = epd7in5b_HD.EPD()

    logging.info("read image file")
    BImage = Image.open('/cfg/output_b.bmp')
    CImage = Image.open('/cfg/output_c.bmp')
        
    logging.info("init")
    epd.init()
    
    if random.choices([True, False], weights=[1, 5]).pop():
        logging.info("clear")
        epd.Clear()
    
    logging.info("display image")
    epd.display(epd.getbuffer(BImage), epd.getbuffer(CImage))
    logging.info("drawing done.")
    
    logging.info("Goto Sleep...")
    epd.sleep()
    epd.Dev_exit()
        
except IOError as e:
    logging.info(e)
    
except KeyboardInterrupt:    
    logging.info("ctrl + c:")
    epd7in5b_HD.epdconfig.module_exit()
    exit()
