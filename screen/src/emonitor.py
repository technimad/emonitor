import sqlite3
from string import Template
import pytz
from pytz import timezone
import matplotlib.dates as mdates
from datetime import datetime
from dateutil.rrule import *
from PIL import Image, ImageDraw, ImageFont
import matplotlib.ticker as ticker
from matplotlib import patheffects
import numpy as np

import time



EPD_WIDTH       = 880
EPD_HEIGHT      = 528

conn = sqlite3.connect('/cfg/p1reader.db')
conn.execute('''attach "/cfg/SBFspot.db" as sbf; ''')

def get_energy_data(bucket_size, bucket_count, bucket_offset=0):
  """Get energy data for display
  The data from different sources will be normalized to buckets. 
  A bucket is a time interval over which the energy data is calculated.
  The unit for a bucket is seconds. So if you want to report based on 5 minute
  intervals, you need buckets of size 300 (5 * 60 seconds).

  arguments:
  bucket_size   -- time interval to calculate the data for, in seconds
  bucket_count  -- the number of buckets to return, integer
  bucket_offset -- by default only the most recent buckets are returned (offset 0), 
                   to get data from the past, increase the offset. 

  returns a tuple:
  (timestamps, net_delivered, solar_used, net_received)
  Energy data is returned as W, unless the bucket_size is larger than 3600 in that case
  the unit is Wh
  Each element of the tuple is an array with length bucket_count.
  The timestamps element consists of datetime objects.
  """
  ts = time.time()
  timestamps = []
  net_delivered = []
  solar_used = []
  net_received = []

  half_bucket = bucket_size // 2  # divide and floor

  if bucket_offset > 0:
    offset = " OFFSET " + str(bucket_offset)
  else:
    offset = ""

  start = bucket_size * bucket_count + (bucket_offset * bucket_size)
  if bucket_size < 3600: # convert Wh to W
    correction = 3600/bucket_size
  else: # no correction needed
    correction = 1
  
  query = Template('''
  SELECT 
  T1.nearestmin         as time, 
  T2.delivered*-1       as net_delivered,
  T1.solar-T2.delivered as solar_used,
  T2.received           as net_received
  FROM (
    SELECT 
    sbfdat.TimeStamp - (sbfdat.TimeStamp % $bucket_size) AS nearestmin, 
    (max(ETotal)-lag(max(ETotal)) over (order by sbfdat.timestamp asc)) * $correction AS solar
    FROM sbf.spotdata as sbfdat 
    WHERE sbfdat.TimeStamp > strftime('%s','now') - $start
    GROUP BY nearestmin
    ORDER BY sbfdat.timestamp desc limit $bucket_count$offset
  ) AS T1
  JOIN (
    SELECT p1dat.timestamp - (p1dat.timestamp % $bucket_size) AS nearestmin, 
    (max(received)-lag(max(received)) over (order by timestamp asc))*1000 * $correction as received, 
    (max(delivered)-lag(max(delivered)) over (order by timestamp asc))*1000 * $correction as delivered
    FROM vwEnergyReadings as p1dat
    WHERE p1dat.timestamp > strftime('%s','now') - $start
    GROUP BY nearestmin
    ORDER BY p1dat.timestamp desc limit $bucket_count$offset
  ) as T2 on T1.nearestmin = T2.nearestmin
  ORDER BY time ASC;
  ''')
  for row in conn.execute(query.substitute(bucket_size=bucket_size,
                                           correction=correction,
                                           half_bucket=half_bucket,
                                           bucket_count=bucket_count,
                                           offset=offset,
                                           start=start)):
    timestamps.append(row[0])
    net_delivered.append(float(row[1] or 0))
    solar_used.append(float(row[2] or 0))
    net_received.append(float(row[3] or 0))

  timestamps = [pytz.utc.localize(datetime.fromtimestamp(d)) for d in timestamps]
  if len(timestamps) == 0:
    print( "no data in timeframe", len(timestamps), len(net_delivered), len(solar_used), len(net_received))
    timestamps = []
    timestamps.append(datetime.now())
  
  print( time.time()-ts, " get_energy_data")  
  return (timestamps, net_delivered, solar_used, net_received)

def get_history_reference_data(bucket_size=900, percentiles=5):
  """Get historical reference sun production data.
  The data from the sun production of the last week,
  The week surrounding today, one year and two years back is used
  to calculate the expected sun production.

  The granularity of the reference data is set by bucket_size.
  A bucket is a time interval over which the energy data is calculated.
  The unit for a bucket is seconds. So if you want to report based on 5 minute
  intervals, you need buckets of size 300 (5 * 60 seconds).

  The width of the certainty interval is the number of percentiles minus two.
  The upper and lower percentile block is discarded.
  Number of percentiles defaults to 5 (blocks of 20 percitile).
  This gives us a certainty interval of 60 percentile. 

  arguments:
  bucket_size  -- time interval to calculate the data for, in seconds 
                  (defaults to 900, 15 mins.)
  percentiles  -- the number of total percentile slices to be used for
                  certainty interval calculation

  returns a tuple:
  (timestamps, net_delivered, solar_used, net_received)
  Each element of the tuple is an array with length bucket_count.
  The timestamps element consists of datetime objects.
  """
  ts = time.time()
  timestamps = []
  pct20s = []
  avgs = []
  pct80s = []

  query = Template('''
  -- [6] get 20pct avg 80pct for each mins
  select min(solar) as pct20,  
         avg, 
         max(solar) as pct80, 
         strftime('%s', 'now', 'start of day')+(mins*60) as timestamp
  from (

  -- [5] select maximum solar production per percentile per mins
  select p, avg, mins, max(solar) as solar
  from (

  -- [4] change moving average to max average per mins
  select p, max(avg_win) over win as avg, mins, solar
  from (

  -- [3] add percentile groups and moving average per mins
  select ntile($percentiles) over win as p, avg(solar) over win as avg_win, mins, solar
  from (

  -- [2] add minutes of day 
  select 
    strftime('%Y%j', nearestmin) as day,
    strftime('%H', nearestmin) * 60 + strftime('%M', nearestmin) as mins,
    solar
    from (

  -- [1] compute production per 10 mins
  select 
    datetime(CASE WHEN (sbfdat.TimeStamp % $bucket_size) < ($bucket_size / 2)
      THEN sbfdat.TimeStamp - (sbfdat.TimeStamp % $bucket_size)
      ELSE sbfdat.TimeStamp - (sbfdat.TimeStamp % $bucket_size) + $bucket_size
      END, 'unixepoch', 'localtime') AS nearestmin, 
    (max(ETotal)-lag(max(ETotal)) over (order by sbfdat.timestamp asc))*3600/$bucket_size as solar
  from spotdata as sbfdat 
  where 
  (sbfdat.timestamp < CAST(strftime('%s' , 'now') as INTEGER) - $bucket_size AND sbfdat.timestamp > strftime('%s', date('now', '-7 day')))
  OR
  (sbfdat.timestamp < strftime('%s' , date('now',  '-1 year', '+4 day')) AND sbfdat.timestamp > strftime('%s', date('now', '-1 year', '-3 day')))
  OR 
  (sbfdat.timestamp < strftime('%s' , date('now',  '-2 year', '+4 day')) AND sbfdat.timestamp > strftime('%s', date('now', '-2 year', '-3 day')))

  group by nearestmin
  order by sbfdat.timestamp desc
  -- end [1]
  )
  -- end [2]
  ) 
  where solar > 0 
  window win as (partition by mins order by solar asc) 
  order by mins
  -- end [3]
  )
  window win as (partition by mins order by solar desc)
  -- end [4]
  )
  group by mins, p
  --end [5]
  )
  where p > 1 AND p < $percentiles
  group by mins
;
  ''')
  max_sane_value = 10000
  for row in conn.execute(query.substitute(bucket_size=bucket_size,
                                           percentiles=percentiles)):
    if row[0] < max_sane_value and row[1] < max_sane_value and row[2] < max_sane_value :
      timestamps.append(row[3])
      pct20s.append(row[0])
      avgs.append(row[1])
      pct80s.append(row[2])

  timestamps = [pytz.utc.localize(datetime.fromtimestamp(d)) for d in timestamps]
  print( time.time()-ts, " get_history_reference_data")  
  return (timestamps, pct20s, avgs, pct80s)

def get_current_production_current_day(bucket_size=900) :
  ts = time.time()
  timestamps = []
  actuals = []

  query = Template('''
  select
    nearestmin as timestamp,
    solar as solar
  from (

  select 
    CASE WHEN (sbfdat.TimeStamp % $bucket_size) < ($bucket_size / 2)
      THEN sbfdat.TimeStamp - (sbfdat.TimeStamp % $bucket_size)
      ELSE sbfdat.TimeStamp - (sbfdat.TimeStamp % $bucket_size) + $bucket_size
      END AS nearestmin, 
    (max(ETotal)-lag(max(ETotal)) over (order by sbfdat.timestamp asc))*3600/$bucket_size as solar
  from spotdata as sbfdat 
  where 
  sbfdat.timestamp > strftime('%s' , date('now',  'start of day'))

  group by nearestmin
  order by sbfdat.timestamp desc

  )
  where solar > 0 
  AND timestamp <= CAST(strftime('%s' , 'now') as INTEGER) - $bucket_size
  ''')
  max_sane_value = 10000
  for row in conn.execute(query.substitute(bucket_size=bucket_size)):
    if row[1] is not None and row[1] < max_sane_value :
      timestamps.append(row[0])
      actuals.append(row[1])
  
  timestamps = [pytz.utc.localize(datetime.fromtimestamp(d)) for d in timestamps]
  print( time.time()-ts, " get_current_production_current_day")  
  return (timestamps, actuals)

def predict_sun(days=30):
  ts = time.time()
  query = Template('''
  select 	strftime('%s', 'now', 'start of day')+(start_slope * $tomorrow + start_intercept) as start_time,
          strftime('%s', 'now', 'start of day')+(max_slope * $tomorrow + max_intercept) as max_time,
          strftime('%s', 'now', 'start of day')+(end_slope * $tomorrow + end_intercept) as end_time
  from (

  select	start_slope,
          start_bar_max - day_bar_max * start_slope as start_intercept,
          max_slope,
          max_bar_max - day_bar_max * max_slope as max_intercept,
          end_slope,
          end_bar_max - day_bar_max * end_slope as end_intercept
  from(


  select	sum((day - day_bar) * (start - start_bar)) / sum((day - day_bar) * (day - day_bar)) as start_slope,
          sum((day - day_bar) * (max - max_bar)) / sum((day - day_bar) * (day - day_bar)) as max_slope,
          sum((day - day_bar) * (end - end_bar)) / sum((day - day_bar) * (day - day_bar)) as end_slope,
          max(day_bar) as day_bar_max,
          max(start_bar) as start_bar_max,
          max(max_bar) as max_bar_max,
          max(end_bar) as end_bar_max
  from (


  Select	day, avg(day) over () as day_bar,
          start, avg(start) over () as start_bar,
          max, avg(max) over () as max_bar,
          end, avg(end) over () as end_bar
  from (

  SELECT	day,  
          start-strftime('%s', datetime(datetime(start, 'unixepoch'), 'start of day')) as start,
          max-strftime('%s', datetime(datetime(start, 'unixepoch'), 'start of day')) as max, 
          end-strftime('%s', datetime(datetime(start, 'unixepoch'), 'start of day')) as end, 
          count 
  FROM(
    SELECT 	timestamp, 
            daycount as day, 
            prod, 
            maxprod, 
            start, 
            max(timestamp) FILTER (where prod = maxprod) over (partition by day order by timestamp asc) as max, 
            end, 
            row_number() over (partition by day order by timestamp desc) as count 
    FROM (
      SELECT 	nearestmin as timestamp, 
              day, 
              prod, 
              max(prod) over win as maxprod, 
              first_value(timestamp) over win as start, 
              last_value(timestamp) over win as end,
              dense_rank() over (order by day) as daycount
      FROM (
        SELECT 	timestamp,
                TimeStamp - (TimeStamp % 60) AS nearestmin, 
                strftime('%Y%j', datetime(timestamp, 'unixepoch')) as day, 
                max(etotal)-lag(max(etotal)) over (order by timestamp asc) as prod
        FROM spotdata 
        WHERE timestamp > strftime('%s', date('now', 'start of day', '-$days day')) 
        AND timestamp < strftime('%s', date('now', 'start of day'))
        GROUP BY nearestmin
        ORDER BY timestamp desc
      ) 
      WHERE prod !=0 
      WINDOW win AS (partition by day order by timestamp asc)
    )
  ) WHERE count =1

  ))));
  ''')
  for row in conn.execute(query.substitute(days=days,
                                           tomorrow=days + 1)):
    print( time.time()-ts, " get_sun_prediction")
    return (pytz.utc.localize(datetime.fromtimestamp(row[0])), 
            pytz.utc.localize(datetime.fromtimestamp(row[1])),
            pytz.utc.localize(datetime.fromtimestamp(row[2])))


def draw_graph(ax, title, timestamps, net_delivered, solar_used, net_received):
  ts = time.time()
  start = mdates.date2num(timestamps[0])
  end = mdates.date2num(timestamps[len(timestamps) - 1])
  bar_width = ((end - start) / len(timestamps)) / 1.3
  
  ax.set_title(title, fontname="Liberation Mono", fontweight="normal")

  ax.yaxis.set_major_formatter(ticker.EngFormatter(places=1, sep=""))

  nd = ax.bar(timestamps, net_delivered, width=bar_width, align="edge", color="red")
  su = ax.bar(timestamps, solar_used, width=bar_width, align="edge", color="red")
  nr = ax.bar(timestamps, net_received, width=bar_width, align="edge", bottom=solar_used, color="black")

  locator = mdates.AutoDateLocator(minticks=1, maxticks=8)
  locator.intervald[MINUTELY] = [15, 30]
  
  formatter = mdates.AutoDateFormatter(locator)
  ax.get_xaxis().set_major_locator(locator)
  ax.get_xaxis().set_major_formatter(formatter)

  ax.xaxis_date()

  ax.axhline(linewidth=0.5, color="black")

  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)
  ax.spines['bottom'].set_visible(False)
  ax.spines['left'].set_visible(False)

  print(time.time() - ts," draw_graph")
  return (nd, su, nr)

def draw_reference(ax, title, timestamps1, lows, avgs, highs, timestamps2, actuals):
  ts = time.time()


  ax.set_title(title, fontname="Liberation Mono", fontweight="normal")

  ax.yaxis.set_major_formatter(ticker.EngFormatter(places=1, sep=""))

  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)
  ax.spines['bottom'].set_visible(False)
  ax.spines['left'].set_visible(False)

  ax.fill_between(timestamps1, lows, highs, hatch="xxxx", facecolor="white")
  ax.plot(timestamps1, avgs, color="black", linewidth=1,)
  ax.plot(timestamps2, actuals, color="red", linewidth=4, path_effects=[patheffects.withStroke(linewidth=6,foreground="w")])

  locator = mdates.AutoDateLocator(minticks=1, maxticks=8)
  locator.intervald[MINUTELY] = [15, 30]
  
  formatter = mdates.AutoDateFormatter(locator)
  ax.get_xaxis().set_major_locator(locator)
  ax.get_xaxis().set_major_formatter(formatter)

  ax.xaxis_date()

  print(time.time() - ts," draw_reference")

def draw_sun(ax, title, sun_up, sun_max, sun_down):
  ts = time.time()

  amsterdam = timezone('Europe/Amsterdam')
  sun_up = sun_up.astimezone(amsterdam).strftime('%H:%M')
  sun_max = sun_max.astimezone(amsterdam).strftime('%H:%M')
  sun_down = sun_down.astimezone(amsterdam).strftime('%H:%M')

  ax.set_title(title, fontname="Liberation Mono", fontweight="normal", y=0, pad=0)
  
  ax.axis([0, 2, -1, 2])
  ax.spines['top'].set_visible(False)
  ax.spines['right'].set_visible(False)
  ax.spines['bottom'].set_visible(False)
  ax.spines['left'].set_visible(False)
  ax.get_xaxis().set_major_locator(ticker.NullLocator())
  ax.get_yaxis().set_major_locator(ticker.NullLocator())

  x = np.linspace(0, 2., 100)
  ax.plot(x, np.sin(x*1.6), color="red", linewidth=3)
    
  ax.text(0, -0.5, sun_up+" ↑", ha="left",   size="x-large")
  ax.text(1, 1.3,  sun_max,   ha="center", size="x-large")
  ax.text(2, -0.5, "↓ "+sun_down, ha="right",  size="x-large")
  print(time.time() - ts," draw_sun")



def split_image(filename):
  base, extention = filename.split(".")
  sourceImage = Image.open(filename).convert('RGB')
  BImage = Image.new('1', (EPD_WIDTH, EPD_HEIGHT), 255)
  BImage_data = BImage.load()
  CImage = Image.new('1', (EPD_WIDTH, EPD_HEIGHT), 255)
  CImage_data = CImage.load()

  sourceImage_data = sourceImage.load()
  image_height, image_width = sourceImage.size

  for pixel_y in range(image_height):
      for pixel_x in range(image_width):
          r, g, b, = sourceImage_data[pixel_y, pixel_x]
          if r < 200 and g < 200 and b < 200:
              BImage_data[pixel_y, pixel_x] = 0
          elif r <= 255 and g < 50 and b < 50:
              CImage_data[pixel_y, pixel_x] = 0
          #elif r != 255 or g != 255 or b != 255 :
              #logging.debug(sourceImage_data[pixel_y, pixel_x])

  BImage.save(base+'_b.bmp')
  CImage.save(base+'_c.bmp')
