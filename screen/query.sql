attach "/db/SBFspot.db" as sbf; 
/*
select 
datetime(CASE WHEN (sbfdat.TimeStamp % 60) < 30
  THEN sbfdat.TimeStamp - (sbfdat.TimeStamp % 60)
  ELSE sbfdat.TimeStamp - (sbfdat.TimeStamp % 60) + 60
  END, 'unixepoch', 'localtime') AS sbfdatnearestmin, 
max(ETotal)-lag(max(ETotal)) over (order by sbfdat.timestamp asc),
NULL,
NULL
from sbf.spotdata as sbfdat 
group by sbfdatnearestmin
order by sbfdat.timestamp desc limit 10 OFFSET 400;

select datetime(CASE WHEN (p1dat.timestamp % 60) < 30
  THEN p1dat.timestamp - (p1dat.timestamp % 60)
  ELSE p1dat.timestamp - (p1dat.timestamp % 60) + 60
  END, 'unixepoch', 'localtime') AS nearestmin, 
NULL,
round((max(received)-lag(max(received)) over (order by timestamp asc))*1000, 2) as received, 
round((max(delivered)-lag(max(delivered)) over (order by timestamp asc))*1000, 2) as delivered
from vwEnergyReadings as p1dat
group by nearestmin
order by p1dat.timestamp desc limit 10 OFFSET 400;
*/

select 
T1.nearestmin, 
T2.delivered*-1,
T1.solar-T2.delivered,
T2.received
FROM (
  select 
  datetime(CASE WHEN (sbfdat.TimeStamp % 60) < 30
    THEN sbfdat.TimeStamp - (sbfdat.TimeStamp % 60)
    ELSE sbfdat.TimeStamp - (sbfdat.TimeStamp % 60) + 60
    END, 'unixepoch', 'localtime') AS nearestmin, 
  max(ETotal)-lag(max(ETotal)) over (order by sbfdat.timestamp asc) as solar
  from sbf.spotdata as sbfdat 
  group by nearestmin
  order by sbfdat.timestamp desc limit 20
) AS T1
JOIN (
  select datetime(CASE WHEN (p1dat.timestamp % 60) < 30
    THEN p1dat.timestamp - (p1dat.timestamp % 60)
    ELSE p1dat.timestamp - (p1dat.timestamp % 60) + 60
    END, 'unixepoch', 'localtime') AS nearestmin, 
  round((max(received)-lag(max(received)) over (order by timestamp asc))*1000, 2) as received, 
  round((max(delivered)-lag(max(delivered)) over (order by timestamp asc))*1000, 2) as delivered
  from vwEnergyReadings as p1dat
  group by nearestmin
  order by p1dat.timestamp desc limit 20
) as T2 on T1.nearestmin = T2.nearestmin;

/*
CREATE VIEW vwEnergyReadings AS
SELECT
strftime('%s', TimeStamp) as TimeStamp,
received_1+received_2 as received,
delivered_1+delivered_2 as delivered
from EnergyReadings
order by timestamp desc;
*/

/*
docker run --rm -it -v $PWD/cfg:/db sqlite3 /db/p1reader.db "`cat query.sql`"

*/

EXPLAIN QUERY PLAN

SELECT 
datetime(nearestmin, 'unixepoch', 'localtime') as nearestmin,
solar-lag(solar) over (order by nearestmin asc) as solar
FROM(
SELECT 
    CASE WHEN (sbfdat.TimeStamp % 300) < 150
      THEN sbfdat.TimeStamp - (sbfdat.TimeStamp % 300)
      ELSE sbfdat.TimeStamp - (sbfdat.TimeStamp % 300) + 300
      END AS nearestmin, 
    max(ETotal) AS solar
    FROM sbf.spotdata as sbfdat 
    WHERE sbfdat.TimeStamp > strftime('%s','now') - 6000
    GROUP BY nearestmin
    ORDER BY sbfdat.timestamp desc limit 20
)

EXPLAIN QUERY PLAN    SELECT 
    datetime(CASE WHEN (sbfdat.TimeStamp % 300) < 150
      THEN sbfdat.TimeStamp - (sbfdat.TimeStamp % 300)
      ELSE sbfdat.TimeStamp - (sbfdat.TimeStamp % 300) + 300
      END, 'unixepoch', 'localtime') AS nearestmin, 
    max(ETotal)-lag(max(ETotal)) over win AS solar
    FROM sbf.spotdata as sbfdat 
    WHERE sbfdat.TimeStamp > strftime('%s','now') - 6000
    GROUP BY nearestmin

    WINDOW win as (ORDER BY sbfdat.TimeStamp asc)
        ORDER BY nearestmin desc limit 20;
    
docker run -it -v $PWD/screen/src:/app -v $PWD/cfg:/cfg screen python3 plot.py
   
 docker run -it -v $PWD/screen/src:/app -v $PWD/cfg:/cfg --privileged screen python3 screen.py

select 	time(start_slope * 31 + start_intercept, 'unixepoch', 'localtime') as start_time,
        time(max_slope * 31 + max_intercept, 'unixepoch', 'localtime') as max_time,
        time(end_slope * 31 + end_intercept, 'unixepoch', 'localtime') as end_time
from (

select	start_slope,
        start_bar_max - day_bar_max * start_slope as start_intercept,
        max_slope,
        max_bar_max - day_bar_max * max_slope as max_intercept,
        end_slope,
        end_bar_max - day_bar_max * end_slope as end_intercept
from(


select	sum((day - day_bar) * (start - start_bar)) / sum((day - day_bar) * (day - day_bar)) as start_slope,
        sum((day - day_bar) * (max - max_bar)) / sum((day - day_bar) * (day - day_bar)) as max_slope,
        sum((day - day_bar) * (end - end_bar)) / sum((day - day_bar) * (day - day_bar)) as end_slope,
        max(day_bar) as day_bar_max,
        max(start_bar) as start_bar_max,
        max(max_bar) as max_bar_max,
        max(end_bar) as end_bar_max
from (


Select	day, avg(day) over () as day_bar,
        start, avg(start) over () as start_bar,
        max, avg(max) over () as max_bar,
        end, avg(end) over () as end_bar
from (

SELECT	day,  
        start-strftime('%s', datetime(datetime(start, 'unixepoch'), 'start of day')) as start,
        max-strftime('%s', datetime(datetime(start, 'unixepoch'), 'start of day')) as max, 
        end-strftime('%s', datetime(datetime(start, 'unixepoch'), 'start of day')) as end, 
        count 
FROM(
  SELECT 	timestamp, 
          daycount as day, 
          prod, 
          maxprod, 
          start, 
          max(timestamp) FILTER (where prod = maxprod) over (partition by day order by timestamp asc) as max, 
          end, 
          row_number() over (partition by day order by timestamp desc) as count 
  FROM (
    SELECT 	nearestmin as timestamp, 
            day, 
            prod, 
            max(prod) over win as maxprod, 
            first_value(timestamp) over win as start, 
            last_value(timestamp) over win as end,
            dense_rank() over (order by day) as daycount
    FROM (
      SELECT 	timestamp,
              TimeStamp - (TimeStamp % 60) AS nearestmin, 
              strftime('%Y%j', datetime(timestamp, 'unixepoch')) as day, 
              max(etotal)-lag(max(etotal)) over (order by timestamp asc) as prod
      FROM spotdata 
      WHERE timestamp > strftime('%s', date('now', 'start of day', '-30 day')) 
      AND timestamp < strftime('%s', date('now', 'start of day'))
      GROUP BY nearestmin
      ORDER BY timestamp desc
    ) 
    WHERE prod !=0 
    WINDOW win AS (partition by day order by timestamp asc)
  )
) WHERE count =1

))));


select
  timeinday,
  min,
  max
  from (
    select
      timeinday,
      min(prod) as min,
      max(prod) as max
    from (
      select 
        TimeStamp - (TimeStamp % 600) AS nearestmin, 
        TimeStamp - (TimeStamp % 600) - strftime('%s', datetime(datetime(TimeStamp, 'unixepoch'), 'start of day')) AS timeinday,  
        max(etotal)-lag(max(etotal)) over 
          (order by timestamp asc) as prod 
      from spotdata 
      where Timestamp > strftime('%s', 'now') - 60482592000 
      GROUP BY nearestmin 
      ORDER BY timestamp desc
    ) 
    WHERE prod > 0
    GROUP BY timeinday
  )
  WHERE max > 0;



select
timeinday ,
 row_number() OVER win    AS row_number,
       rank() OVER win          AS rank,
       dense_rank() OVER win    AS dense_rank,
       percent_rank() OVER win  AS percent_rank,
       cume_dist() OVER win     AS cume_dist
from(
      select 
        TimeStamp - (TimeStamp % 600) AS nearestmin, 
        TimeStamp - (TimeStamp % 600) - strftime('%s', datetime(datetime(TimeStamp, 'unixepoch'), 'start of day')) AS timeinday,  
        max(etotal)-lag(max(etotal)) over 
          (order by timestamp asc) as prod 
      from spotdata 
      where Timestamp > strftime('%s', 'now') - 60482592000 
      GROUP BY nearestmin 
      ORDER BY timestamp desc
)
WINDOW win AS (ORDER BY timeinday);




select 
   max(solar), avg(solar), min(solar), mins, day
  from (

select 
strftime('%Y%j', nearestmin) as day,
    strftime('%H', nearestmin) * 60 + strftime('%M', nearestmin) as mins,
    solar
    FROM (

select 
  datetime(CASE WHEN (sbfdat.TimeStamp % 600) < 300
    THEN sbfdat.TimeStamp - (sbfdat.TimeStamp % 600)
    ELSE sbfdat.TimeStamp - (sbfdat.TimeStamp % 600) + 600
    END, 'unixepoch', 'localtime') AS nearestmin, 
  max(ETotal)-lag(max(ETotal)) over (order by sbfdat.timestamp asc) as solar
  from spotdata as sbfdat 
  where sbfdat.timestamp < strftime('%s' , date('now',  '-3 day')) AND sbfdat.timestamp > strftime('%s', date('now', '-13 day'))
  group by nearestmin
  order by sbfdat.timestamp desc

  )
  ) group by mins






  -- [6] get 20pct avg 80pct for each mins
select min(solar) as pct20,  
          avg, 
          max(solar) as pct80, 
          	strftime('%s', 'now', 'start of day')+(mins*60) as timestamp
from (

-- [5] select maximum solar production per percentile per mins
select p, avg, mins, max(solar) as solar
from (

-- [4] change moving average to max average per mins
select p, max(avg_win) over win as avg, mins, solar
from (

-- [3] add percentile groups and moving average per mins
select ntile(8) over win as p, avg(solar) over win as avg_win, mins, solar
                  from (

-- [2] add minutes of day 
select 
    strftime('%Y%j', nearestmin) as day,
    strftime('%H', nearestmin) * 60 + strftime('%M', nearestmin) as mins,
    solar
    FROM (

-- [1] compute production per 10 mins
select 
  datetime(CASE WHEN (sbfdat.TimeStamp % 600) < 300
    THEN sbfdat.TimeStamp - (sbfdat.TimeStamp % 600)
    ELSE sbfdat.TimeStamp - (sbfdat.TimeStamp % 600) + 600
    END, 'unixepoch', 'localtime') AS nearestmin, 
  max(ETotal)-lag(max(ETotal)) over (order by sbfdat.timestamp asc) as solar
  from spotdata as sbfdat 
  where 
  (sbfdat.timestamp < strftime('%s' , date('now',  'start of day')) AND sbfdat.timestamp > strftime('%s', date('now', '-7 day')))
  OR
  (sbfdat.timestamp < strftime('%s' , date('now',  '-1 year', '+3 day')) AND sbfdat.timestamp > strftime('%s', date('now', '-1 year', '-3 day')))
  OR 
    (sbfdat.timestamp < strftime('%s' , date('now',  '-2 year', '+3 day')) AND sbfdat.timestamp > strftime('%s', date('now', '-2 year', '-3 day')))

  group by nearestmin
  order by sbfdat.timestamp desc
-- end [1]
  )
-- end [2]
  ) 
  where solar > 0 
 window win as (partition by mins order by solar asc) 
 order by mins
-- end [3]
 )
 window win as (partition by mins order by solar desc)
 -- end [4]
 )
 group by mins, p
 --end [5]
 )
 where p > 1 AND p < 8
 group by mins
;