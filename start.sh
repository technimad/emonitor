#!/usr/bin/env bash
set -e          # Abort script at first error, when a command exits with non-zero status 
set -o pipefail # Causes a pipeline to return the exit status of the last command in the 
                # pipe that returned a non-zero return value.
set -u          # Attempt to use undefined variable outputs error message, and forces an exit

DEBUG=${DEBUG:-}
if [[ ! -z $DEBUG ]]; then
	set -x
fi

cd `dirname $0`

# setup symlink to usb port for P1 connection
if [[ ! -L /dev/ttyP1  ]]; then
  sudo ln -s /dev/serial/by-id/usb-FTDI_FT232R_USB_UART* /dev/ttyP1
fi
readlink /dev/ttyP1

# create docker compose file with right interval in healthcheck
interval=$(grep -o "^\(RunInterval=\)[0-9]\+$" cfg/SBFspot.cfg | cut -d "=" -f2)
hcinterval=$((interval * 4))
sed "s:__HCINT__:$hcinterval:g" utils/templates/docker-compose.yml.template > docker-compose.yml

docker-compose pull || /bin/true
docker-compose up -d --no-build

