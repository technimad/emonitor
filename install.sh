#!/usr/bin/env bash
set -e          # Abort script at first error, when a command exits with non-zero status 
set -o pipefail # Causes a pipeline to return the exit status of the last command in the 
                # pipe that returned a non-zero return value.
set -u          # Attempt to use undefined variable outputs error message, and forces an exit

DEBUG=${DEBUG:-}
if [[ ! -z $DEBUG ]]; then
	set -x
fi

cd `dirname $0`
thisdir=`pwd`

echo "# create unit file for service restarter"
sed "s:__DIR__:$thisdir:g" utils/templates/emonitor-healthcheck.template > utils/templates/emonitor-healthcheck.service

echo "# create unit file for the main service"
sed "s:__DIR__:$thisdir:g" utils/templates/emonitor.template > utils/templates/emonitor.service

echo "# create timer services"
CTZ=$(grep ^Timezone cfg/SBFspot.cfg | cut -d"=" -f2)
sed "s:__CTZ__:$CTZ:g" utils/templates/emonitor-screen.timer.template >  utils/templates/emonitor-screen.timer
sed "s:__DIR__:$thisdir:g" utils/templates/emonitor-screen.service.template > utils/templates/emonitor-screen.service

echo "# copy unit files to system"
sudo cp -f utils/templates/emonitor-healthcheck.service /etc/systemd/system/
sudo cp -f utils/templates/emonitor.service /etc/systemd/system/
sudo cp -f utils/templates/emonitor-screen.timer /etc/systemd/system/
sudo cp -f utils/templates/emonitor-screen.service /etc/systemd/system/

echo "# enable units"
sudo systemctl enable emonitor.service
sudo systemctl enable emonitor-healthcheck.service
sudo systemctl enable emonitor-screen.timer

echo "# check router ip mac config"
if [[ -f cfg/router-ip-mac.cfg ]]; then
  echo "* config file found"
  source cfg/router-ip-mac.cfg
  if [[ ! -z $ROUTER_IP ]] && [[ ! -z $ROUTER_MAC ]]; then
    echo "* config file read"
    COMMAND="/sbin/ip neigh add $ROUTER_IP lladdr $ROUTER_MAC dev eth0"
    if ! grep -q "$COMMAND" /etc/rc.local; then
      sudo sed -i "/exit 0/s/^/#/" /etc/rc.local
      echo $COMMAND >> /etc/rc.local
      echo "+ added command to rc.local"
    else
      echo "- command already installed in rc.local"
    fi
  fi
fi

# screen containers have NET access, creating a veth interface when they start.
# dhcpcd sometimes gets confused and removes the default route fo eth- instead.
# veth interfaces don't need an ip address, denying dhcpcd these interfaces fixes this issue.
echo "# deny veth interfaces for dhcpcd" 
if ! grep -q "denyinterfaces veth*" /etc/dhcpcd.conf; then
  echo "* adding config to dhcpcd.conf"
  echo "denyinterfaces veth*" >> /etc/dhcpcd.conf
  echo "* restart dhcpcd unit"
  sudo systemctl daemon-reload
  sudo systemctl restart dhcpcd
else
  echo "* config already present."	
fi

echo "# start units"
sudo systemctl start emonitor.service
sudo systemctl start emonitor-healthcheck.service
sudo systemctl start emonitor-screen.timer
