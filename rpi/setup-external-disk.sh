#!/usr/bin/env bash

# adafruit-pi-externalroot-helper
# 
# Configure a Raspbian system to use an external USB drive as root filesystem.
#
# See README.md for details and sources.

set -e

function print_version() {
    echo "Adafruit Pi External Root Helper v0.1.0"
    exit 1
}

function print_help() {
    echo "Usage: $0 -d [target device]"
    echo "    -h            Print this help"
    echo "    -v            Print version information"
    echo "    -d [device]   Specify path of device to convert to root"
    echo
    echo "You must specify a device. See:"
    echo "https://learn.adafruit.com/external-drive-as-raspberry-pi-root"
    exit 1
}


# Display an error message and quit:
function bail() {
    FG="1;31m"
    BG="40m"
    echo -en "[\033[${FG}\033[${BG}error\033[0m] "
    echo "$*"
    exit 1
}

# Display an info message:
function info() {
    task="$1"
    shift
    FG="1;32m"
    BG="40m"
    echo -e "[\033[${FG}\033[${BG}${task}\033[0m] $*"
}

if [[ $EUID -ne 0 ]]; then
    bail "must be run as root. try: sudo adafruit-pi-externalroot-helper"
fi

# Handle arguments:
args=$(getopt -uo 'hvd:' -- $*)
[ $? != 0 ] && print_help
set -- $args

for i
do
    case "$i"
    in
        -h)
            print_help
            ;;
        -v)
            print_version
            ;;
        -d)
            target_drive="$2"
            echo "Target drive = ${2}"
            shift
            shift
            ;;
    esac
done

if [[ ! -e "$target_drive" ]]; then
    bail "Target ${target_drive} must be existing device (most common option to use is -d /dev/sda)"
fi

info "start" "Will create new ext4 filesystem on ${target_drive}"
info "start" "If there is data on ${target_drive}, it will be lost."
read -p "Really proceed? (y)es / (n)o " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "Quitting."
    exit
fi

export root_partition="${target_drive}1"
export data_partition="${target_drive}2"

info "dependencies" "Installing gdisk, rsync, and parted."
    # All except gdisk are probably installed, but let's make sure.
    apt-get install -y gdisk rsync parted

info "fs create" "Creating ${root_partition}"
    # The alternative here seems to be to pipe a series of commands
    # to fdisk(1), similar to how it's done by raspi-config:
    # https://github.com/asb/raspi-config/blob/3a5d75340a1f9fe5d7eebfb28fee0e24033f4fd3/raspi-config#L68
    # This seemed to work, but I was running into weirdness because
    # that doesn't seem to make a GPT, and later on I couldn't get
    # partition unique GUID from gdisk.  parted(1) is also a nice
    # option because it's scriptable and allows partition sizes to
    # be specified in percentages.
    parted --script "${target_drive}" mklabel gpt
    parted --script --align optimal "${target_drive}" mkpart primary ext4 0% 10G
info "fs create" "Creating ${data_partition}"
    parted --script --align optimal "${target_drive}" mkpart data ext4 10G 100%
    sleep 5
    
info "fs create" "Creating root ext4 filesystem on ${root_partition}"
    mkfs -t ext4 -L rootfs "${root_partition}"
    tune2fs -o journal_data "${root_partition}"
info "fs create" "Creating data ext4 filesystem on ${data_partition}"
    mkfs -t ext4 -L data "${data_partition}"
    tune2fs -o journal_data "${data_partition}"

info "fs id" "Getting UUID for root partition"
    eval `blkid -o export "${root_partition}"`
    export root_uuid=$UUID
    export root_partuuid=$PARTUUID
    info "fs id" "Root partition UUID: ${root_uuid}"
    info "fs id" "Root unique GUID: ${root_partuuid}"
info "fs id" "Getting UUID for data partition"
    eval `blkid -o export "${data_partition}"`
    export data_uuid=$UUID
    export data_partuuid=$PARTUUID
    info "fs id" "Data partition UUID: ${data_uuid}"
    info "fs id" "Data unique GUID: ${data_partuuid}"


info "fs copy" "Mounting ${root_partition} on /mnt"
    mount "${root_partition}" /mnt

info "fs copy" "Copying root filesystem to ${root_partition} with rsync"
info "fs copy" "This will take quite a while.  Please be patient!"
    rsync -ax --info=progress2 / /mnt

info "boot config" "Configuring boot from {$root_partition}"
    # rootdelay=5 is likely not necessary here, but seems to do no harm.
    cp /boot/cmdline.txt /boot/cmdline.txt.bak
    sed -i "s|PARTUUID=.* rootfs|PARTUUID=${root_partuuid} rootfs|" /boot/cmdline.txt

info "boot config" "Commenting out old root partition in /etc/fstab, adding new one"
    # These changes are made on the new drive after copying so that they
    # don't have to be undone in order to switch back to booting from the
    # SD card.
    #sed -i '/mmcblk0p2/s/^/#/' /mnt/etc/fstab
    sed -i "/ext4/s/^/#/" /mnt/etc/fstab 
    echo "PARTUUID=${root_partuuid}    /   ext4    defaults,noatime  0       1" >> /mnt/etc/fstab
    echo "PARTUUID=${data_partuuid}    /data   ext4    defaults,noatime  0       1" >> /mnt/etc/fstab

info "boot config" "Ok, your system should be ready. You may wish to check:"
info "boot config" "  /mnt/etc/fstab"
info "boot config" "  /boot/cmdline.txt"
info "boot config" "Your new root drive is currently accessible under /mnt."
info "boot config" "In order to restart with this drive at /, please type:"
info "boot config" "sudo reboot"