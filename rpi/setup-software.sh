#!/bin/env bash
timedatectl set-timezone UTC
timedatectl
apt-get update
apt-get install -y \
     git \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg-agent \
     software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
docker run hello-world
/usr/sbin/usermod -aG docker pi
apt-get install -y docker-compose
sudo chmod -x /usr/bin/docker-credential-secretservice

apt-get install -y libftdi-dev

# wget  http://ftp.us.debian.org/debian/pool/main/libs/libseccomp/libseccomp2_2.5.4-1+b3_armhf.deb || error "Failed to download libseccomp2.deb"
sudo dpkg -i vendor/libseccomp2_2.5.4-1+b3_armhf.deb || error "Failed to install libseccomp2.deb"
