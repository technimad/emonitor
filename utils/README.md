Some interesting commands run during development:

```
watch -n 30 "docker run --rm -it -v \"$PWD\"/cfg:/cfg sqlite3:latest cfg/SBFspot.db \"select nearest5min,PacTot,max(EToday) as max, max(EToday)-lag(max(EToday)) over (order by timestamp asc) as pdelta from vwSpotData group by nearest5min order by timestamp desc limit 30;\""
```

```
docker run --rm  spark:latest `docker run --rm -v "$PWD"/cfg:/cfg sqlite3:latest cfg/SBFspot.db "select max(EToday)-lag(max(EToday)) over (order by timestamp asc) as pmax from vwSpotData group by nearest5min order by timestamp desc limit 50;" | tac | tr '\n' ' '`
```

```
while true; do docker run --rm  spark:latest `docker run --rm -v "$PWD"/cfg:/cfg sqlite3:latest cfg/SBFspot.db "select max(EToday)-lag(max(EToday)) over (order by timestamp asc) as pmax from vwSpotData group by nearest5min order by timestamp desc limit 50;" | tac | tr '\n' ' '`; sleep 300; done;
```

```
while true; do docker run --rm  spark:latest `docker run --rm -v "$PWD"/cfg:/cfg sqlite3:latest cfg/SBFspot.db "select EToday-lag(EToday) over (order by timestamp asc) as pmax from vwSpotData order by timestamp desc limit 50;" | tac | tr '\n' ' '`; sleep 20; done;
```

All commands are run from within the emonitor dir.