#!/bin/bash
./utils/backup &
ssh rpi docker run -t --rm -v /home/pi/emonitor/screen/src:/app -v /home/pi/emonitor/cfg:/cfg registry.gitlab.com/technimad/emonitor/screen:v.1.2 python3 plot.py &
wait < <(jobs -p)
./utils/backup
qlmanage -p ./cfg/output.png
